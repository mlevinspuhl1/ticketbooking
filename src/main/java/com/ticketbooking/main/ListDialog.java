/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.CustomerDTO;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Employee;
import com.tbcommons.model.Event;
import com.tbcommons.model.Ticket;
import com.tbcommons.model.User;
import com.tbcommons.model.Venue;
import com.ticketbooking.main.business.CustomerBusiness;
import com.ticketbooking.main.business.EventBusiness;
import com.ticketbooking.main.business.TicketBusiness;
import com.ticketbooking.main.business.UserBusiness;
import com.ticketbooking.main.business.VenueBusiness;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 *
 */
public class ListDialog extends javax.swing.JDialog {

    private static final Logger Log = LogManager.getLogger(ListDialog.class);
    private static final long serialVersionUID = 2884308154403774335L;
    private List<User> userList;
    private List<Venue> venueList;
    private List<Event> eventList;
    private List<Ticket> ticketList;
    private User user;
    private String typeList;

    /**
     *
     * @param user
     * @param typeList
     * @param parent
     * @param modal
     */
    public ListDialog(User user, String typeList, java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
        this.user = user;
        this.typeList = typeList;
        initComponents();
        setGUI(typeList);
        fillTitle(typeList);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtbUser = new javax.swing.JTable(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        jPanel4 = new javax.swing.JPanel();
        btnEdit = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnPay = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 236, 209));

        jPanel2.setBackground(new java.awt.Color(239, 255, 239));

        jtbUser.setModel(getDefaultTableModel(typeList));
        jtbUser.getColumnModel().getColumn(0).setMinWidth(0);
        jtbUser.getColumnModel().getColumn(0).setPreferredWidth(0);
        jtbUser.getColumnModel().getColumn(0).setMaxWidth(0);
        jtbUser.getColumnModel().getColumn(1).setPreferredWidth(220);
        jtbUser.getColumnModel().getColumn(1).setMaxWidth(600);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        jScrollPane1.setViewportView(jtbUser);
        jScrollPane1.setHorizontalScrollBarPolicy(
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jScrollPane1.setVerticalScrollBarPolicy(
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jtbUser.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(239, 255, 239));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnEdit.setText("Edit");
        btnEdit.setMaximumSize(new java.awt.Dimension(100, 23));
        btnEdit.setMinimumSize(new java.awt.Dimension(100, 23));
        btnEdit.setPreferredSize(new java.awt.Dimension(100, 23));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnClose.setText("Close");
        btnClose.setMaximumSize(new java.awt.Dimension(100, 23));
        btnClose.setMinimumSize(new java.awt.Dimension(100, 23));
        btnClose.setPreferredSize(new java.awt.Dimension(100, 23));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.setMaximumSize(new java.awt.Dimension(100, 23));
        btnAdd.setMinimumSize(new java.awt.Dimension(100, 23));
        btnAdd.setPreferredSize(new java.awt.Dimension(100, 23));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.setMaximumSize(new java.awt.Dimension(100, 23));
        btnDelete.setMinimumSize(new java.awt.Dimension(100, 23));
        btnDelete.setPreferredSize(new java.awt.Dimension(100, 23));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnPay.setText("Pay");
        btnPay.setMaximumSize(new java.awt.Dimension(100, 23));
        btnPay.setMinimumSize(new java.awt.Dimension(100, 23));
        btnPay.setPreferredSize(new java.awt.Dimension(100, 23));
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(239, 255, 239));

        lbTitle.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("title");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int row = jtbUser.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, PLEASE_SELECT_ONE_ROW);
            return;
        }

        switch (typeList) {
            case ADMIN:
                getRegisterDialog();
                break;
            case VENUE:
                Long idVenue = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
                Venue venue = venueList.stream().filter(e -> e.getId() == idVenue).findFirst().get();
                openVenueRegister(venue);
                break;
            case EVENT:
                Long idEvent = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
                Event event = eventList.stream().filter(e -> e.getId() == idEvent).findFirst().get();
                openEventRegister(event);
                break;
            case TICKET:
                Long idTicket = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
                Ticket ticket = ticketList.stream().filter(e -> e.getId() == idTicket).findFirst().get();
                openTicketRegister(ticket);
                break;
        }

        updateTableModel(typeList);

    }//GEN-LAST:event_btnEditActionPerformed
    private void getRegisterDialog() {

        Long id = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
        User user = userList.stream().filter(e -> e.getId() == id).findFirst().get();

        RegisterDialog frame = new RegisterDialog(user, ADMIN, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Admin");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);

    }
    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        switch (typeList) {
            case EMPLOYEE:
                selectUserPaqe(this.user);
                break;
            case VENUE:
                selectUserPaqe(this.user);
                break;
            case EVENT:
                selectUserPaqe(this.user);
                break;
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        switch (typeList) {
            case VENUE:
                deleteVenue();
                break;
            case EVENT:
                deleteEvent();
                break;
            case TICKET:
                deleteTicket();
                break;

        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed

        ticketList.removeIf(x -> x.getPaymentId() != null);
        if (ticketList.isEmpty()) {
            CustomerDTO dto = CustomerBusiness.getInstance()
                    .getCustomerById(user.getCustomerList().get(0).getId());
            ticketList = dto.getTicketList();
            JOptionPane.showMessageDialog(this,
                    "Ticket already paid",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        PaymentDialog frame = new PaymentDialog(ticketList, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Payment");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        CustomerDTO dto = CustomerBusiness.getInstance()
                .getCustomerById(user.getCustomerList().get(0).getId());
        ticketList = dto.getTicketList();
        jtbUser.setModel(getTicketDTM());
    }//GEN-LAST:event_btnPayActionPerformed

    private boolean confirmDelete(String message) {
        int input = JOptionPane.showConfirmDialog(null,
                message, SELECT_AN_OPTION,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        return input != 2;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnPay;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtbUser;
    private javax.swing.JLabel lbTitle;
    // End of variables declaration//GEN-END:variables

    private DefaultTableModel getDefaultTableModel(String typeList) {

        switch (typeList) {
            case ADMIN:
                return getAdminDTM();
            case VENUE:
                return getVenueDTM();
            case EVENT:
                return getEventDTM();
            case TICKET:
                return getTicketDTM();
            default:
                return null;

        }
    }

    private DefaultTableModel getAdminDTM() {
        userList = UserBusiness.getInstance().getAllUsers();
        String[] columnNames = {"id", "Email", "User Type", "Name", "Surname"};

        DefaultTableModel dtm = new DefaultTableModel() {
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Long.class;

                    case 1:
                        return String.class;

                    case 2:
                        return String.class;

                    case 3:
                        return String.class;

                    case 4:
                        return String.class;
                    default:
                        return String.class;
                }
            }
        };
        dtm.setColumnIdentifiers(columnNames);

        for (User user : userList) {
            dtm.addRow(new Object[]{user.getId(), user.getEmail(), user.getType(),
                user.getFirstName(), user.getLastName()});
        }
        return dtm;
    }

    private void updateTableModel(String typeList) {

        jtbUser.setModel(getDefaultTableModel(typeList));
        jtbUser.getColumnModel().getColumn(0).setMinWidth(0);
        jtbUser.getColumnModel().getColumn(0).setPreferredWidth(0);
        jtbUser.getColumnModel().getColumn(0).setMaxWidth(0);
        jtbUser.getColumnModel().getColumn(1).setPreferredWidth(220);
        jtbUser.getColumnModel().getColumn(1).setMaxWidth(600);
        jScrollPane1.setViewportView(jtbUser);
        jScrollPane1.setHorizontalScrollBarPolicy(
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        jScrollPane1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    }

    private void selectUserPaqe(User user) {
        try {
            if (user == null) {
                throw new ValidationException("user null");
            }
            String profile = user.getType();

            switch (profile) {
                case EMPLOYEE: ;
                    openEmployeeRegister(user.getEmployeeList().get(0));
                    break;
                case ARTIST: ;
                    openArtistRegister(user.getArtistList().get(0));
                    break;
            }
        } catch (ValidationException ex) {
            Log.error("selectUserPaqe", ex);
        }
    }

    private void openEmployeeRegister(Employee emp) {
        VenueRegisterDialog frame
                = new VenueRegisterDialog(emp,
                        new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Venue");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        updateTableModel(typeList);
    }

    private void openArtistRegister(Artist artist) {
        EventRegisterDialog frame
                = new EventRegisterDialog(artist,
                        new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Event");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        updateTableModel(typeList);
    }

    private DefaultTableModel getVenueDTM() {
        String[] columnNames = {"id", "Description", "Venue Name", "Owner Name", "Capacity"};
        venueList = VenueBusiness.getInstance().getAll();
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Long.class;

                    case 1:
                        return String.class;

                    case 2:
                        return String.class;

                    case 3:
                        return String.class;

                    case 4:
                        return Integer.class;
                    default:
                        return String.class;
                }
            }
        };
        dtm.setColumnIdentifiers(columnNames);
        for (Venue venue : venueList) {
            dtm.addRow(new Object[]{venue.getId(), venue.getDescription(), venue.getName(),
                (venue.getOwnerName()), venue.getCapacity()});
        }
        return dtm;
    }

    private void openVenueRegister(Venue venue) {
        VenueRegisterDialog frame
                = new VenueRegisterDialog(false, true, venue,
                        new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Venue");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        getVenueDTM();
    }

    private void deleteVenue() {
        int row = jtbUser.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, PLEASE_SELECT_ONE_ROW);
            return;
        }
        if (!confirmDelete(CONFIRM_DELETE_VENUE)) {
            return;
        }
        Long id = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
        boolean result = VenueBusiness.getInstance().delete(id);
        if (result) {

        }
        updateTableModel(typeList);
    }

    private DefaultTableModel getEventDTM() {
        String[] columnNames = {"id", "Event type", "Event date", "Event Category"};
        eventList = EventBusiness.getInstance()
                .getByArtistID(user.getArtistList().get(0).getId());
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Long.class;

                    case 1:
                        return String.class;

                    case 2:
                        return String.class;

                    case 3:
                        return String.class;
                    default:
                        return String.class;
                }
            }
        };
        dtm.setColumnIdentifiers(columnNames);
        for (Event event : eventList) {
            dtm.addRow(new Object[]{event.getId(), event.getType(),
                TicketBusiness.getInstance().getStringDate(event.getDate()),
                (event.getCategory())});
        }
        return dtm;
    }

    private void openEventRegister(Event event) {
        EventRegisterDialog frame
                = new EventRegisterDialog(false, event,
                        new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Event");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        getVenueDTM();
    }

    private void deleteEvent() {
        int row = jtbUser.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, PLEASE_SELECT_ONE_ROW);
            return;
        }
        if (!confirmDelete(CONFIRM_DELETE_EVENT)) {
            return;
        }
        Long id = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
        boolean result = EventBusiness.getInstance().delete(id);
        if (result) {

        }
        updateTableModel(typeList);
    }

    private void fillTitle(String typeList) {
        String title = typeList;
        switch (typeList) {
            case ADMIN:
                title = "User";
                break;
        }
        lbTitle.setText(title);
    }

    private DefaultTableModel getTicketDTM() {
        String[] columnNames = {"id", "Category", "Date", "Type",
            "Price", "Quantity", "Total", "Status", "Printed"};
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Long.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                    case 3:
                        return String.class;
                    case 4:
                        return BigDecimal.class;
                    case 5:
                        return String.class;
                    case 6:
                        return BigDecimal.class;
                    case 7:
                        return String.class;
                    case 8:
                        return String.class;
                    default:
                        return String.class;
                }
            }
        };
        dtm.setColumnIdentifiers(columnNames);

        CustomerDTO dto = CustomerBusiness.getInstance()
                .getCustomerById(user.getCustomerList().get(0).getId());
        ticketList = dto.getTicketList();
        if (ticketList != null) {
            for (Ticket ticket : ticketList) {
                dtm.addRow(new Object[]{
                    ticket.getId(),
                    ticket.getEventId().getCategory(),
                    TicketBusiness.getInstance()
                    .getStringDate(ticket.getEventId().getDate()),
                    ticket.getType(), ticket.getPrice() + "€", ticket.getTicketQuantity(),
                    calcTotal(ticket.getPrice(), ticket.getTicketQuantity()) + "€",
                    ticket.getPaymentId() == null ? "Not Paid" : "Paid",
                     ticket.getTicketPrinted()?"Yes":"No"});
            }
        }
        return dtm;
    }

    private String calcTotal(BigDecimal price, int quantity) {
        BigDecimal quant = new BigDecimal(quantity);
        return TicketBusiness.getInstance()
                .bigDecimalToString(price.multiply(quant));
    }

    private void setGUI(String typeList) {
        switch (typeList) {
            case TICKET:
                btnAdd.setVisible(false);
                btnEdit.setVisible(false);
                break;
            default:
                btnPay.setVisible(false);
        }
    }

    private void deleteTicket() {

        int row = jtbUser.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, PLEASE_SELECT_ONE_ROW);
            return;
        }
        Long id = (Long) jtbUser.getValueAt(jtbUser.getSelectedRow(), 0);
        Ticket ticket = ticketList.stream().filter(e -> e.getId() == id)
                .findFirst().get();
        if (ticket.getPaymentId() != null) {
            JOptionPane.showMessageDialog(this,
                    "Paid ticket can not be deleted",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (!confirmDelete(CONFIRM_DELETE_TICKET)) {
            return;
        }
        Boolean result = TicketBusiness.getInstance().delete(id);
        if (result) {
            JOptionPane.showMessageDialog(this, TICKET_DELETED);
        } else {
            JOptionPane.showMessageDialog(this, CONTACT_ADMIN, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        jtbUser.setModel(getTicketDTM());

    }

    private void openTicketRegister(Ticket ticket) {
        if (ticket.getPaymentId() != null) {
            JOptionPane.showMessageDialog(this,
                    "A paid ticket can not be updated",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        TicketDialog frame = new TicketDialog(ticket,
                new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Ticket");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);

        getTicketDTM();

    }
}
