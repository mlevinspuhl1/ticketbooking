/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.UserDTO;
import com.tbcommons.model.User;
import com.ticketbooking.main.business.UserBusiness;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.bind.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 *
 */
public class LoginFrame extends javax.swing.JFrame {

    private static final long serialVersionUID = -3447197436941702556L;
    private static final Logger Log = LogManager.getLogger(LoginFrame.class);
    private static JFrame frame;

    /**
     * Creates new form Login
     */
    public LoginFrame() {
        initComponents();
        tfEmail.setText(UserBusiness.getInstance().getPreferenceEmail());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        tfLoginError = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        tfEmail = new javax.swing.JTextField();
        lbEmail = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfEmailError = new javax.swing.JTextField();
        tfPasswordError = new javax.swing.JTextField();
        jPasswordField = new javax.swing.JPasswordField();
        jPanel4 = new javax.swing.JPanel();
        btnRegister = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();

        jButton3.setText("jButton3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(255, 236, 209));

        jPanel1.setBackground(new java.awt.Color(239, 255, 239));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tfLoginError.setEditable(false);
        tfLoginError.setBackground(new java.awt.Color(239, 255, 239));
        tfLoginError.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfLoginError.setBorder(null);
        tfLoginError.setFocusable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfLoginError)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(tfLoginError, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(239, 255, 239));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbEmail.setText("Email");

        jLabel4.setText("Password");

        tfEmailError.setEditable(false);
        tfEmailError.setBackground(new java.awt.Color(239, 255, 239));
        tfEmailError.setBorder(null);
        tfEmailError.setFocusable(false);

        tfPasswordError.setEditable(false);
        tfPasswordError.setBackground(new java.awt.Color(239, 255, 239));
        tfPasswordError.setBorder(null);
        tfPasswordError.setFocusable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(tfEmailError))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(lbEmail))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfEmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(tfPasswordError, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE))
                            .addComponent(jPasswordField, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE))))
                .addGap(48, 48, 48))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbEmail)
                    .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfEmailError, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfPasswordError, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(239, 255, 239));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnRegister.setText("Register");
        btnRegister.setAlignmentY(0.0F);
        btnRegister.setMaximumSize(new java.awt.Dimension(100, 23));
        btnRegister.setMinimumSize(new java.awt.Dimension(100, 23));
        btnRegister.setPreferredSize(new java.awt.Dimension(100, 23));
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        btnLogin.setText("Login");
        btnLogin.setAlignmentY(0.0F);
        btnLogin.setMaximumSize(new java.awt.Dimension(100, 23));
        btnLogin.setMinimumSize(new java.awt.Dimension(100, 23));
        btnLogin.setPreferredSize(new java.awt.Dimension(100, 23));
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnClose.setText("Close");
        btnClose.setMaximumSize(new java.awt.Dimension(100, 23));
        btnClose.setMinimumSize(new java.awt.Dimension(100, 23));
        btnClose.setPreferredSize(new java.awt.Dimension(100, 23));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(56, Short.MAX_VALUE)
                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(163, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (InputValidate()) {

            writeTextFieldMessage(tfLoginError, "", Color.RED);
            try {
                UserDTO dto = UserBusiness.getInstance()
                        .doLogin(tfEmail.getText().trim(),
                                //jPasswordField.getText().trim());
                String.valueOf(jPasswordField.getPassword()).trim());
                if (dto == null) {

                    writeTextFieldMessage(tfLoginError, CONTACT_ADMIN, Color.RED);
                    return;
                }
                User user = dto.getUser();
                if (user == null) {
                    writeTextFieldMessage(tfLoginError, dto.getMessage(), Color.RED);
                    return;
                }
                if (user.getType() == null) {
                    //Next common user windows
                } else {
                    //Next userType windows
                }
                UserBusiness.getInstance().setPreferenceEmail(user.getEmail());
                user = UserBusiness.getInstance().addUserTypeEntity(user, dto);
                selectUserPaqe(user);
            } catch (Exception ex) {
                Log.error("btnLoginActionPerformed", ex);
                writeTextFieldMessage(tfLoginError, CONTACT_ADMIN, Color.RED);
            } finally {
                setNormalCursor();
            }
        }
    }//GEN-LAST:event_btnLoginActionPerformed
    private void goToAdmin(User user) {
        ListDialog frame = new ListDialog(user, ADMIN, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Admin");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();
    }
    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        RegisterDialog frame = new RegisterDialog(null, REGISTER, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Register");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();
    }//GEN-LAST:event_btnRegisterActionPerformed

    private void goToUser(User user) {
        UserDialog frame = new UserDialog(user, USER, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking User");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();

    }
    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnCloseActionPerformed

    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws javax.swing.UnsupportedLookAndFeelException
     */
    public static void main(String args[]) throws ClassNotFoundException,
            InstantiationException,
            IllegalAccessException,
            UnsupportedLookAndFeelException {
        Log.info("The main() method is called");
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        //SwingUtilities.invokeLater(new Runnable() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels = javax.swing.UIManager.getInstalledLookAndFeels();
//            for (UIManager.LookAndFeelInfo installedLookAndFeel : installedLookAndFeels) {
//                if ("Nimbus".equals(installedLookAndFeel.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(installedLookAndFeel.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame = new LoginFrame();
                frame.setTitle("Ticket Booking Login");
                URL url = getClass().getResource("/image/Tickets-32.png");
                ImageIcon img = new ImageIcon(url);
                frame.setIconImage(img.getImage());
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {
                        //save preferences

                    }

                });
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPasswordField jPasswordField;
    private javax.swing.JLabel lbEmail;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfEmailError;
    private javax.swing.JTextField tfLoginError;
    private javax.swing.JTextField tfPasswordError;
    // End of variables declaration//GEN-END:variables

    private boolean InputValidate() {

        String email = tfEmail.getText().trim();
        String password = String.valueOf(jPasswordField.getPassword()).trim();
        tfEmailError.setText("");
        tfPasswordError.setText("");
        if (email.isEmpty()) {
            tfEmailError.setForeground(Color.RED);
            tfEmailError.setText("email is required");
            return false;
        }
        if (!UserBusiness.getInstance().isValidEmailAddress(email)) {
            tfEmailError.setForeground(Color.RED);
            tfEmailError.setText("wrong email format");
            return false;

        }
        if (password.isEmpty()) {
            tfPasswordError.setForeground(Color.RED);
            tfPasswordError.setText("password is required");
            return false;
        }
        return true;
    }

    private void writeTextFieldMessage(JTextField textField, String message, Color color) {
        textField.setText(message);
        textField.setForeground(color);
    }

   

    private void selectUserPaqe(User user) {
        if (user == null) {
            Log.error("selectUserPaqe", new ValidationException("user null"));
        }
        String profile = user.getType() == null || !user.getProfileActive()
                ? USER : user.getType();

        switch (profile) {
            case ADMIN: ;
                goToAdmin(user);
                break;
            case EMPLOYEE: ;
                goToEmplooyee(user);
                break;
            case CUSTOMER: ;
                goToCustomer(user);
                break;
            case ARTIST: ;
                goToArtist(user);
                break;
            case USER: ;
                goToUser(user);
                break;
        }
    }

    private void goToArtist(User user) {
        ArtistDialog frame = new ArtistDialog(user, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Artist");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();
    }

    private void goToEmplooyee(User user) {
        EmployeeDialog frame = new EmployeeDialog(user, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Employee");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();
    }

    private void setNormalCursor() {
        frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    private void goToCustomer(User user) {
        CustomerDialog frame = new CustomerDialog(user, new javax.swing.JDialog(), true);
        frame.setTitle("Ticket Booking Customer");
        URL url = getClass().getResource("/image/Tickets-32.png");
        ImageIcon img = new ImageIcon(url);
        frame.setIconImage(img.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.setVisible(true);
        setNormalCursor();
    }

}
