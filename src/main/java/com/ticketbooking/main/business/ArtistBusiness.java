/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.ArtistDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Artist;
import static com.ticketbooking.main.business.Business.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author
 */
public class ArtistBusiness extends Business{
    
   private static ArtistBusiness instance;

    private ArtistBusiness() {

    }

    public static ArtistBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new ArtistBusiness();
        }
        return instance;
    }

    public ArtistDTO saveOrUpdate(Artist artist) {
        if (artist.getId() == null) {
            return save(artist);
        } else {
            return update(artist);
        }
    }

    public ArtistDTO update(Artist artist) {
        try {
            ValidationArtist.update(artist);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(ARTIST_UPDATE_PATH)
                    .path(artist.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(artist, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, ArtistDTO.class);
        } catch (ValidationException ex) {
            Log.error("update", ex);
            return new ArtistDTO(FAIL, ex.getMessage());
        } catch (IOException ex) {
            Log.error("update", ex);
            return new ArtistDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public ArtistDTO save(Artist artist) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(ARTIST_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(artist, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, ArtistDTO.class);

        } catch (IOException ex) {
            Log.error("save", ex);
            return new ArtistDTO(FAIL, CONTACT_ADMIN);
        }

    }

    public List<Artist> getAll() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<Artist> artistList = client.target(URL_API).path(ARTIST_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<Artist>>() {
                    });
            return artistList;
        } catch (Exception ex) {
            Log.error("getAll", ex);
            return new ArrayList<>();
        }
    }

    public boolean delete(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API)
                    .path(EVENT_REMOVE_API_PATH).path(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .delete(Boolean.class);
        } catch (Exception ex) {
            Log.error("delete", ex);
            return false;
        }

    }

}

