/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Venue;
import java.math.BigDecimal;
import javax.xml.bind.ValidationException;

public class ValidationVenue {

    public static void update(Venue venue) throws ValidationException {
        if (venue == null) {
            throw new ValidationException("Venue is required");
        }
        if (venue.getAddress1().isEmpty()) {
            throw new ValidationException("Address1 is required");
        }
        if (venue.getCost().compareTo(BigDecimal.ZERO)==0) {
            throw new ValidationException("Cost is required");
        }
        if (venue.getEircode().isEmpty()) {
            throw new ValidationException("Postal code is required");
        }
        if (venue.getName().isEmpty()) {
            throw new ValidationException("Venue name is required");
        }
        if (venue.getOwnerName().isEmpty()) {
            throw new ValidationException("Owner is required");
        }
        if (venue.getPhoneNumber().isEmpty()) {
            throw new ValidationException("Phone number is required");
        }
        if (venue.getCapacity() == 0) {
            throw new ValidationException("Venue capacity is required");
        }
    }

}
