/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.VenueDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Venue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

public class VenueBusiness extends Business {

    private static VenueBusiness instance;

    private VenueBusiness() {

    }

    public static VenueBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new VenueBusiness();
        }
        return instance;
    }

    public VenueDTO saveOrUpdate(Venue venue) {
        if (venue.getId() == null) {
            return save(venue);
        } else {
            return update(venue);
        }
    }

    public VenueDTO update(Venue venue) {
        try {
            ValidationVenue.update(venue);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(VENUE_UPDATE_PATH)
                    .path(venue.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(venue, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, VenueDTO.class);
        } catch (ValidationException ex) {
            Log.error("update", ex);
            return new VenueDTO(FAIL, ex.getMessage());
        } catch (IOException ex) {
            Log.error("doUpdate", ex);
            return new VenueDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public VenueDTO save(Venue venue) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(VENUE_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(venue, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, VenueDTO.class);

        } catch (IOException ex) {
            Log.error("save", ex);
            return new VenueDTO(FAIL, CONTACT_ADMIN);
        }

    }

    public List<Venue> getAll() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<Venue> venueList = client.target(URL_API).path(VENUE_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<Venue>>() {
                    });
            return venueList;
        } catch (Exception ex) {
            Log.error("getAll", ex);
            return new ArrayList<>();
        }
    }

    public boolean delete(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API)
                    .path(VENUE_REMOVE_API_PATH).path(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .delete(Boolean.class);
        } catch (Exception ex) {
            Log.error("delete", ex);
            return false;
        }

    }

}
