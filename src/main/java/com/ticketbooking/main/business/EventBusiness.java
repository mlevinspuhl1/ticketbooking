/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.EventDTO;
import com.tbcommons.dto.UserDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Event;
import static com.ticketbooking.main.business.Business.Log;
import static com.ticketbooking.main.business.Business.auth;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author
 */
public class EventBusiness extends Business {

    private static EventBusiness instance;

    private EventBusiness() {

    }

    public static EventBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new EventBusiness();
        }
        return instance;
    }

    public EventDTO saveOrUpdate(Event event) {
        if (event.getId() == null) {
            return save(event);
        } else {
            return update(event);
        }
    }

    public EventDTO update(Event event) {
        try {
            ValidationEvent.update(event);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(EVENT_UPDATE_PATH)
                    .path(event.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(event, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, EventDTO.class);
        } catch (ValidationException ex) {
            Log.error("update", ex);
            return new EventDTO(FAIL, ex.getMessage());
        } catch (IOException ex) {
            Log.error("doUpdate", ex);
            return new EventDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public EventDTO save(Event event) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(EVENT_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(event, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, EventDTO.class);

        } catch (IOException ex) {
            Log.error("save", ex);
            return new EventDTO(FAIL, CONTACT_ADMIN);
        }

    }

    public List<Event> getAll() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<Event> eventList = client.target(URL_API).path(EVENT_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<Event>>() {
                    });
            return eventList;
        } catch (Exception ex) {
            Log.error("getAll", ex);
            return new ArrayList<>();
        }
    }

    public boolean delete(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API)
                    .path(EVENT_REMOVE_API_PATH).path(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .delete(Boolean.class);
        } catch (Exception ex) {
            Log.error("delete", ex);
            return false;
        }

    }

    public List<Event> getByArtistID(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API).path(EVENT_ID_ARTIST_API_PATH)
                    .path(id.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<List<Event>>() {
                    });
        } catch (Exception ex) {
            Log.error("getByArtistID", ex);
            return new ArrayList<>();
        }
    }

}
