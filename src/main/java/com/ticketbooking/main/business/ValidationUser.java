/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.User;
import javax.xml.bind.ValidationException;

public class ValidationUser {

    public static void UserRegister(User user, boolean admin) throws Exception {

        if (user.getEmail().isEmpty()) {
            throw new ValidationException("Email is required");
        }
        if (!UserBusiness.getInstance().isValidEmailAddress(user.getEmail())) {
            throw new ValidationException("Wrong email format");
        }
        if (user.getPassword().isEmpty()) {
            throw new ValidationException("Password is required");
        }
        if (user.getFirstName().isEmpty()) {
            throw new ValidationException("Name is required");
        }
        if (user.getLastName().isEmpty()) {
            throw new ValidationException("Surname is required");
        }
        if (user.getPhoneNumber().isEmpty()) {
            throw new ValidationException("Phone is required");
        }
        if (user.getAddress1().isEmpty()) {
            throw new ValidationException("Address1 is required");
        }
        if (user.getCity().isEmpty()) {
            throw new ValidationException("City is required");
        }
        if (user.getCounty().isEmpty()) {
            throw new ValidationException("County is required");
        }
        if (user.getCountry().isEmpty()) {
            throw new ValidationException("Country is required");
        }
        if (user.getPostCode().isEmpty()) {
            throw new ValidationException("Postal Code is required");
        }
        if (user.getType()==null) {
            throw new ValidationException("User type is required");
        }
        if(admin && user.getStatus()==null){
            throw new ValidationException("User status is required");
        }

    }

}
