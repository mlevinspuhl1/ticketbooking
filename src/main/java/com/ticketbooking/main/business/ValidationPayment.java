/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Payment;
import javax.xml.bind.ValidationException;

/**
 *
 * @author genef
 */
public class ValidationPayment {
    
     public static void update(Payment payment) throws ValidationException {
        if (payment == null){
            throw new ValidationException("Payment is required");            
        }
        if (payment.getType().isEmpty()){
            throw new ValidationException("Payment type is required");            
        }
        if (payment.getDate()==null){
            throw new ValidationException("Payment date is required");            
        }
        if (payment.getTotal() == null){
            throw new ValidationException("Paymet Total is required");            
        }
    }
    
}
