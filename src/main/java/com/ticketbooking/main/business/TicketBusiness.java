/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.TicketDTO;
import com.tbcommons.dto.TicketPaymentDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Ticket;
import static com.ticketbooking.main.business.Business.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author
 */
public class TicketBusiness extends Business {

    private static TicketBusiness instance;

    private TicketBusiness() {

    }

    public static TicketBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new TicketBusiness();
        }
        return instance;
    }

    public TicketDTO saveOrUpdate(Ticket ticket) {
        if (ticket.getId() == null) {
            return save(ticket);
        } else {
            return update(ticket);
        }
    }

    public TicketDTO update(Ticket ticket) {
        try {
            ValidationTicket.update(ticket);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(TICKET_UPDATE_PATH)
                    .path(ticket.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(ticket, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, TicketDTO.class);
        } catch (ValidationException ex) {
            Log.error("update", ex);
            return new TicketDTO(FAIL, ex.getMessage());
        } catch (IOException ex) {
            Log.error("update", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public TicketDTO save(Ticket ticket) {
        try {
            ValidationTicket.update(ticket);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(TICKET_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(ticket, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, TicketDTO.class);
        } catch (ValidationException ex) {
            Log.error("save", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        } catch (IOException ex) {
            Log.error("save", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public TicketDTO saveAll(TicketPaymentDTO dto) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(TICKET_SAVE_ALL_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(dto, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, TicketDTO.class);
        } catch (IOException ex) {
            Log.error("save", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public List<Ticket> getAll() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<Ticket> ticketList = client.target(URL_API).path(TICKET_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<Ticket>>() {
                    });
            return ticketList;
        } catch (Exception ex) {
            Log.error("getAll", ex);
            return new ArrayList<>();
        }
    }

    public boolean delete(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API)
                    .path(TICKET_REMOVE_API_PATH).path(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .delete(Boolean.class);
        } catch (Exception ex) {
            Log.error("delete", ex);
            return false;
        }

    }

    public String bigDecimalToString(BigDecimal value) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        value = value.setScale(2, BigDecimal.ROUND_UP);
        DecimalFormat df = new DecimalFormat("#.##",
                DecimalFormatSymbols.getInstance(Locale.US));
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setGroupingUsed(false);
        return df.format(value);

    }

    public String getStringDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);
    }

    public TicketDTO updatePrintedList(List<Long> ids) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(TICKET_UPDATE_PRINTED_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(ids, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, TicketDTO.class);
        } catch (IOException ex) {
            Log.error("updatePrintedList", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        }
    }
}
