/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Event;
import javax.xml.bind.ValidationException;

/**
 *
 * @author genef
 */
public class ValidationEvent {

    public static void update(Event event) throws ValidationException {
        if (event == null) {
            throw new ValidationException("Event is required");
        }
        if (event.getType().isEmpty()) {
            throw new ValidationException("Event type is required");
        }
        if (event.getDate() == null) {
            throw new ValidationException("Event date is required");
        }
        if (event.getCategory().isEmpty()) {
            throw new ValidationException("Event category is required");
        }
        if (event.getAdultTicketPrice() == null) {
            throw new ValidationException("Adult price is required");
        }
        if (event.getElderlyTicketPrice() == null) {
            throw new ValidationException("Elderly price is required");
        }
        if (event.getKIDSTicketPrice() == null) {
            throw new ValidationException("Kids price is required");
        }
        if (event.getStudentTicketPrice() == null) {
            throw new ValidationException("Student price is required");
        }
        if (event.getTeenagerTicketPrice() == null) {
            throw new ValidationException("Teenager price is required");
        }
        if (event.getVenueId() == null) {
            throw new ValidationException("Venue is required");
        }

    }

}
