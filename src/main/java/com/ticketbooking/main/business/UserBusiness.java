/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.UserDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import static com.tbcommons.enums.Enums.UserStatusEnum.*;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Employee;
import com.tbcommons.model.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.prefs.Preferences;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

public class UserBusiness extends Business {

    private static UserBusiness instance;
    private final Preferences prefs;

    private UserBusiness() {
        prefs = Preferences.userNodeForPackage(com.ticketbooking.main.business.UserBusiness.class);
    }

    public static UserBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new UserBusiness();
        }
        return instance;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public String encodingLogin(String email, String password) {
        try {
            String originalInput = email + ":" + password;
            return Base64.getEncoder().encodeToString(originalInput.getBytes());
        } catch (Exception ex) {
            Log.fatal("encodingLogin", ex);
            return null;
        }
    }

    public UserDTO doLogin(String email, String password) {
        try {
            String encrypted = encodingLogin(email, password);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(USER_LOGIN_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .post(Entity.entity(encrypted, MediaType.TEXT_PLAIN_TYPE), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);
        } catch (IOException ex) {
            Log.error("doLogin", ex);
            return null;
        }
    }

    public UserDTO doRegister(User user, boolean admin) {
        user.setStatus(INACTIVE.label);
        try {
            ValidationUser.UserRegister(user, admin);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(USER_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);
        } catch (ValidationException ex) {
            Log.error("doRegister", ex);
            return new UserDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("doRegister", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public boolean sendEmail(UserDTO userDTO) {
        try {
            if (userDTO == null || userDTO.getUser() == null) {
                throw new ValidationException("No user to send");
            }
            String encrypted = encodingLogin(userDTO.getUser().getEmail(),
                    userDTO.getUser().getPassword());
            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
            params.add("key", encrypted);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            client.target(URL_API).path(USER_EMAIL_PATH)
                    .queryParam("key", encrypted)
                    .request(MediaType.APPLICATION_JSON).get(String.class);
            return true;
        } catch (Exception ex) {
            Log.error("sendEmail", ex);
            return false;
        }
    }

    public UserDTO doUpdate(User user, boolean admin) {
        try {
            ValidationUser.UserRegister(user, admin);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(USER_UPDATE_PATH)
                    .path(user.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);
        } catch (ValidationException ex) {
            Log.error("doUpdate", ex);
            return new UserDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("doUpdate", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public UserDTO updateEmployee(Long userId, Employee emp) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(EMPLOYEE_UPDATE_PATH)
                    .path(userId.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(emp, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);

        } catch (IOException ex) {
            Log.error("updateEmployee", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public UserDTO updateArtist(Long id, Artist artist) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(USER_ARTIST_UPDATE_PATH)
                    .path(id.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(artist, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);

        } catch (IOException ex) {
            Log.error("updateArtist", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public UserDTO getUserById(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            UserDTO dto = client.target(URL_API).path(USER_API_PATH)
                    .path(id.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .get(UserDTO.class);
            return dto;
        } catch (Exception ex) {
            Log.error("getUserById", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public List<User> getAllUsers() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<User> userList = client.target(URL_API).path(USER_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<User>>() {
                    });
            return userList;
        } catch (Exception ex) {
            Log.error("getAllUsers", ex);
            return new ArrayList<>();
        }
    }

    public void setPreferenceEmail(String email) {
        try {
            prefs.put(PREFERENCE_EMAIL, email);
        } catch (Exception ex) {
            Log.error("setPreferenceEmail", ex);
        }
    }

    public String getPreferenceEmail() {
        try {
            return prefs.get(PREFERENCE_EMAIL, "");
        } catch (Exception ex) {
            Log.error("getPreferenceEmail", ex);
            return "";
        }

    }

    public User addUserTypeEntity(User user, UserDTO dto) {
        if (dto.getEmployee() != null) {
            List<Employee> empList = new ArrayList<>();
            empList.add(dto.getEmployee());
            user.setEmployeeList(empList);

        }
        if (dto.getCustomer() != null) {
            List<Customer> customerList = new ArrayList<>();
            customerList.add(dto.getCustomer());
            user.setCustomerList(customerList);

        }
        if (dto.getArtist() != null) {
            List<Artist> artistList = new ArrayList<>();
            artistList.add(dto.getArtist());
            user.setArtistList(artistList);

        }
        return user;
    }

    public UserDTO updateCustomer(Long id, Customer customer) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(USER_CUSTOMER_UPDATE_PATH)
                    .path(id.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(customer, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, UserDTO.class);

        } catch (IOException ex) {
            Log.error("updateCustomer", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        }
    }
}
