/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Artist;
import javax.xml.bind.ValidationException;

/**
 *
 * @author
 */
public class ValidationArtist {

    static void update(Artist artist) throws ValidationException {
        if (artist == null) {
            throw new ValidationException("Artist is required");
        }
        if (artist.getArtisticName() == null) {
            throw new ValidationException("Artistic Name is required");
        }
        if (artist.getContactNumber() == null) {
            throw new ValidationException("Contact number is required");
        }
        if (artist.getEventType() == null) {
            throw new ValidationException("Event type is required");
        }
    }

}
