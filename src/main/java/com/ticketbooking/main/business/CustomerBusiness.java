/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.CustomerDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Customer;
import static com.ticketbooking.main.business.Business.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.ValidationException;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author
 */
public class CustomerBusiness extends Business{
    
   private static CustomerBusiness instance;

    private CustomerBusiness() {

    }

    public static CustomerBusiness getInstance() {
        if (instance == null) {
            auth = HttpAuthenticationFeature.basic("gene.fro@hotmail.com", "Familia21");
            instance = new CustomerBusiness();
        }
        return instance;
    }
    public CustomerDTO getCustomerById(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            CustomerDTO dto = client.target(URL_API).path(CUSTOMER_API_PATH)
                    .path(id.toString())
                    .request(MediaType.APPLICATION_JSON)
                    .get(CustomerDTO.class);
            return dto;
        } catch (Exception ex) {
            Log.error("getCustomerById", ex);
            return new CustomerDTO(FAIL, CONTACT_ADMIN);
        }
    }
    public CustomerDTO saveOrUpdate(Customer customer) {
        if (customer.getId() == null) {
            return save(customer);
        } else {
            return update(customer);
        }
    }

    public CustomerDTO update(Customer customer) {
        try {
            ValidationCustomer.update(customer);
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(CUSTOMER_UPDATE_PATH)
                    .path(customer.getId().toString())
                    .request(MediaType.APPLICATION_JSON)
                    .put(Entity.entity(customer, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, CustomerDTO.class);
        } catch (ValidationException ex) {
            Log.error("update", ex);
            return new CustomerDTO(FAIL, ex.getMessage());
        } catch (IOException ex) {
            Log.error("update", ex);
            return new CustomerDTO(FAIL, CONTACT_ADMIN);
        }
    }

    public CustomerDTO save(Customer customer) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            String resp = client.target(URL_API).path(CUSTOMER_SAVE_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(customer, MediaType.APPLICATION_JSON), String.class);
            return new ObjectMapper().readValue(resp, CustomerDTO.class);

        } catch (IOException ex) {
            Log.error("save", ex);
            return new CustomerDTO(FAIL, CONTACT_ADMIN);
        }

    }

    public List<Customer> getAll() {
        try {

            Client client = ClientBuilder.newClient();
            client.register(auth);
            List<Customer> customerList = client.target(URL_API).path(CUSTOMER_API_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<Customer>>() {
                    });
            return customerList;
        } catch (Exception ex) {
            Log.error("getAll", ex);
            return new ArrayList<>();
        }
    }

    public boolean delete(Long id) {
        try {
            Client client = ClientBuilder.newClient();
            client.register(auth);
            return client.target(URL_API)
                    .path(CUSTOMER_REMOVE_API_PATH).path(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .delete(Boolean.class);
        } catch (Exception ex) {
            Log.error("delete", ex);
            return false;
        }

    }

}

