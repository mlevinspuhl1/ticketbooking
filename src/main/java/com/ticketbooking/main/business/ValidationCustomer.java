/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Customer;
import javax.xml.bind.ValidationException;

/**
 *
 * @author
 */
public class ValidationCustomer {

    static void update(Customer customer) throws ValidationException {
        if (customer == null) {
            throw new ValidationException("Customer is required");
        }
        if (customer.getDob() == null) {
            throw new ValidationException("DOB is required");
        }
        if (customer.getGender() == null) {
            throw new ValidationException("Gender is required");
        }
        if (customer.getUserId() == null) {
            throw new ValidationException("User is required");
        }
    }

}
