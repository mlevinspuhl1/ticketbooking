/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.main.business;

import com.tbcommons.model.Ticket;
import javax.xml.bind.ValidationException;

/**
 *
 * @author
 */
public class ValidationTicket {

    static void update(Ticket ticket) throws ValidationException {
        if (ticket == null) {
            throw new ValidationException("Ticket is required");
        }
        if (ticket.getCustomerId() == null) {
            throw new ValidationException("Customer is required");
        }
        if (ticket.getEventId() == null) {
            throw new ValidationException("Event is required");
        }
        if (ticket.getPrice() == null) {
            throw new ValidationException("Price is required");
        }
        if (ticket.getType() == null) {
            throw new ValidationException("Type is required");
        }
    }
}
